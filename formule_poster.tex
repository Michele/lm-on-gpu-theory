\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[hidelinks]{hyperref}
\usepackage{cancel}
%opening
\title{}
\author{Michele Mastropietro}

\DeclareMathOperator{\argmin}{argmin}
\DeclareMathOperator{\diag}{diag}
\begin{document}

% \maketitle

\section{Definitions}

\paragraph{Source spectra $S$:}
Power law dependent on the set of $k$ parameters $\{ \alpha_k\}$:
\[
S(E; \{ \alpha_k\}) = A \left(\frac{E}{E_0}\right) ^{-b}
\]
with $\{ \alpha_k\} = (A, b, E_0)$ which could be simplified to $\{ \alpha_k\} = (A, b) $ by providing energies in a known unit of measure (e.g. GeV or TeV)

\paragraph{Flux densities $J$:}

\[J(E, \vec p; \{ \alpha_k\}) \approx S(E; \{ \alpha_k\})\cdot r(\gamma,s)\]
where $\vec p$ is the photon arrival direction, $E$ the photon energy.\\
$r$ takes into account the source position and the photon direction and it is a known factor for each photon $\gamma$ and source $s$.

\section{Log-likelihood}
\[
\log \mathcal{L} = \sum_i^{n_{ph}}{\log \sum_j^{n_s}J_j(E_i, \vec{p}_i; \{ \alpha_k\}_j)}
\]
where:\\
$n_{ph}$ is the number of events (photons)\\
$n_{s}$ is the number of sources\\
$\{ \alpha_k\}_j$ are the $k$ parameters of the model corresponding to the source $j$.
\section{Levenberg Marquardt algorithm}
Finds the minimizer $\mathbf{x}^* =\underset{\mathbf{x}}{\argmin} \{ F(\mathbf{x})\}$ where:
% \[
% F(\mathbf{x}) = \sum_i{ f_i^2(\mathbf{x})}
% \]
% Heuristically used also with:
\begin{equation}
\label{eq:argmin}
F(\mathbf{x}) = -\sum_i{ \log f_i(\mathbf{x})}
\end{equation}
% \[
% f: \mathbf{x} \mapsto \mathbf{y}, \mathbb{R}^n \rightarrow \mathbb{R}^m\qquad
% \]
% \[
% g: \mathbf{x} \mapsto \mathbf{J}, \mathbb{R}^n \rightarrow \mathbb{R}^{m\times n}
% \]
$\mathbf{x_0}$: initial guess of the solution.\\
$\mathbf{x}_{i+1}=\mathbf{x}_i+\mathbf{h}$\\
Step $\mathbf{h}$ is computed at each iteration by solving the linear system:
% \[
%  \mathbf{A}\cdot\mathbf{h}=-\mathbf{J}^T\mathbf{y}
% \]
\[
 \mathbf{A}\cdot\mathbf{h}=-\mathbf{g}
\]
where:
% \[\mathbf{J}\text{ is the Jacobian of $f$. } J_{i,j}=\frac{\partial f_i}{\partial x_j}\]
\[\mathbf{g}=\mathbf{g}(\mathbf{x})=\mathbf{F}'(\mathbf{x})\]
\[g_k= - \sum_i{\frac{1}{f_i}\frac{\partial f_i}{\partial x_k}}\]
\[\mathbf{A}=\mathbf{H}+\lambda\cdot \diag(\mathbf{H})\]
$\mathbf{H}$ is an approximation\footnote{found in \href{http://seal.web.cern.ch/seal/documents/minuit/mntutorial.pdf}{Minuit tutorial, pag.32}} of the Hessian of the function $F$ ($\mathbf{H}\approx\nabla^2 F$)
\[
H_{p,q}=\sum_k{\frac{1}{f_i^2} \frac{\partial f_i}{\partial x_p} \frac{\partial f_i}{\partial x_q}} \approx \nabla^2 F
\]
This approximation assures positive-definiteness and leaves untouched the symmetry of $A$. For this, the best method to solve the system at each iteration is the Cholesky decomposition.

$\lambda$ is the core parameter of the Levenberg-Marquardt algorithm. It is chosen to be small at the beginning of the process and then it is updated depending on the gain ratio:
\[
\rho=\frac{F(\mathbf{x}+\mathbf{h})-F(\mathbf{x})}{L(0)-L(\mathbf{h})}
\]
where $L(\mathbf{h})$ is a quadratic approximation in the neighbour of $F(\mathbf{x})$:
\[
F(\mathbf{x}+\mathbf{h}) \approx L(\mathbf{h}) = F(\mathbf{x}) + \mathbf{h}^T\nabla F +\frac 1 2 \mathbf{h}^T \nabla^2 F \mathbf{h}
\]

\section{Levenberg-Marquardt to compute Maximum likelihood}
$f_i$ in equation \eqref{eq:argmin} is:
\[f_i(\{ \alpha_k\})= \sum_j^{n_s}J_j(E_i, \vec{p}_i; \{ \alpha_k\}_j)\]

In code these values are collected in the array $\mathbf{y}$:
\[\mathbf{y} = ( f_1, f_2, f_3, \hdots, f_{n_{ph}} )^T\]

We have 20000 events and 50 sources for a total of 100 unknown parameters $x_k$.

\[\{ \alpha_k\} = \{ \alpha_k\}_j \text{ for }j=1..n_s\]
\[\mathbf{x}=\{ \alpha_k\} = ( A_1, b_1, A_2, b_2, \hdots, A_{50},b_{50} )^T\]
Note that:
\begin{align*}
\frac{\partial S}{\partial A} &= \left(\frac{E}{E_0}\right) ^{-b} = \frac{1}{A} \cdot S\\
\frac{\partial S}{\partial b} &= -A\left(\frac{E}{E_0}\right) ^{-b} \cdot \log\left(\frac{E}{E_0}\right) = - S \log\left(\frac{E}{E_0}\right)
\end{align*}
\paragraph{Gradient $\mathbf{g}$}
\begin{align*}
g_k(\mathbf{x}) &= - \sum_i^{n_{ph}}{\frac{1}{f_i}\frac{\partial f_i}{\partial x_k}}=\\
&=- \sum_i^{n_{ph}}{\frac{1}{\sum_j^{n_s}J_j(E_i, \vec{p}_i; \{ \alpha\}_j)}\frac{\partial (\sum_j^{n_s}J_j(E_i, \vec{p}_i; \{ \alpha\}_j))}{\partial x_k}} = \\
&=- \sum_i^{n_{ph}}{\frac{1}{\sum_j^{n_s}S_j(E_i;\{ \alpha\}_j) \cdot r_{i,j}}\frac{\partial (\sum_j^{n_s}S_j(E_i;\{ \alpha\}_j) \cdot r_{i,j})}{\partial x_k}}\\
% &= - \sum_i^{n_{ph}}{\frac{1}{\sum_j^{n_s}S_j(E_i;\{ \alpha\}_j) \cdot r_{i,j}}} 
\end{align*}
So, in case of a parameter in odd position $x_{2n+1}=b_k$ we have:
\begin{align*}
g_{2n+1} =g_k &= - \sum_i^{n_{ph}}{\frac{1}{\sum_j^{n_s}S_j(E_i;\{ \alpha\}_j) \cdot r_{i,j}} \frac{\partial (\sum_j^{n_s}S_j(E_i;\{ \alpha\}_j) \cdot r_{i,j})}{\partial b_k}}=\\
&= - \sum_i^{n_{ph}}{\frac{1}{\sum_j^{n_s}S_j(E_i;\{ \alpha\}_j) \cdot r_{i,j}}\left(-A_k\left(\frac{E_i}{E_0}\right)^{-b_k}\log\left(\frac{E_i}{E_0}\right) \cdot r_{i,k}\right)}=\\
&= - \sum_i^{n_{ph}}{\frac{-S_k(E_i;A_k,b_k)\log\left(\frac{E_i}{E_0}\right)\cdot r_{i,k}}{\sum_j^{n_s}S_j(E_i,\{ \alpha\}_j) \cdot r_{i,j}}}
% &= - \sum_i^{n_{ph}}{\frac{\left(-A_k\left(\frac{E_i}{E_0}\right)^{-b_k}\log\left(\frac{E_i}{E_0}\right) \cdot r_{i,k}\right)}{\sum_j^{n_s}S_j(E_i,\{ \alpha\}_j) \cdot r_{i,j}}}
\end{align*}
Whereas in case of a parameter in even position $x_{2n}=A_k$ we have:
\begin{align*}
g_{2n} =g_k &= - \sum_i^{n_{ph}}{ \frac{1}{A_k} \frac{S_k(E_i;A_k,b_k) \cdot r_{i,k}}{\sum_j^{n_s}S_j(E_i,\{ \alpha\}_j) \cdot r_{i,j}}}
\end{align*}

Gradient $\mathbf{g}$ is a vector of length 100 ($n_s$).
\paragraph{Hessian $\mathbf{H}$}
\[
 H_{p,q}=\sum_i^{n_{ph}}{\frac{1}{f_i^2}\frac{\partial f_i}{\partial x_p} \frac{\partial f_i}{\partial x_q}}
\]

Hessian is a $100\times 100$ matrix ($n_s \times n_s$).
\paragraph{Gain Ratio}
For the Log-Likelihood function $\mathcal{L}(\mathbf{x})$
\[
 \rho =
\]
\section{Implementation}
% \begin{itemize}
%  \item Kernel Levenberg Marquardt to reduce host-device data transfer:
%  \item Kernel computing $J$:
%  Parallelization of the number of photons.
%  \item Kernel computing $\mathbf{g}$
% \end{itemize}

Levenberg-Marquardt algorithm is the de facto standard method to solve problem of least square fitting, but can be used also as a general minimization method provided a way to approximate the second derivative (the hessian) of the function to be minized. In case of a problem of maximum log-likelihood, the second derivative can be easily approximated using the formula shown above (Ref. Minuit Tutorial).

\paragraph{Kernel implementations}
We implemented two kernels to compute the value and the gradient $\mathbf{g}$ of the likelihood.
In both kernels we have used the strategy to parallelize on photons. The inner reduction on sources is done directly inside the kernel, whereas the outer reduction step (summation on photons) is done after the kernel launch but still on the GPU.
We implemented also the Levenberg Marquardt algorithm (LMA) directly on the GPU to reduce host-device data transfer.
By taking advantage of dynamic parallelism offered by devices of compute capability 3.5 or higher, the kernel implementing LMA launches at each iteration the value and the gradient kernels going forward in the minimization process.
The algorithm to solve the linear system at each iteration step has been chosen to be Cholesky decomposition which is the best method to solve system with symmetric and positive definite matrixes of coefficients (ref. Numerical Recipes). 

\section{Other notes}
\begin{itemize}
 \item Role of logarithm
 \item Kernel with a loop inside
\end{itemize}

\end{document}
